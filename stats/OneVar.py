import statistics as stat

#Requires python 3.8
#Current Implementation: https://docs.python.org/3/library/statistics.html

#Measures of Central Location
def mean(data): return stat.mean(data)
def fmean(data): return stat.fmean(data)
def median(data): return stat.median(data)
def median_low(data): return stat.median_low(data)
def median_high(data): return stat.median_high(data)
def mode(data): return stat.mode(data)
def multimode(data): return stat.multimode(data)


#Measures of Spread
#population
def pstdev(data): return stat.pstdev(data)
def pvariance(data): return stat.pvariance(data)
#sample
def stdev(data): return stat.stdev(data)
def variance(data): return stat.variance(data)

#Quartiles
def quartiles(data):
	q1, median, q3 = stat.quantiles(data, n = 4, method = 'exclusive')
	return (q1, median, q3)

def outliersByIQR(data):
	q1, median, q3 = quartiles(data)
	lower_bound = q1 - 1.5*(q3 - q1)
	upper_bound = q3 + 1.5*(q3 - q1)
	outliers = []
	for val in data:
		if val < lower_bound: outliers.append(val)
		elif val > upper_bound: outliers.append(val)
	return outliers
